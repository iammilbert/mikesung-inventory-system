<?php
namespace App\Actions\Authentication;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class LoginUser
{

  public function __construct(array $request)
  {
      $this->request = $request;
  }

  public function execute(){

    $user = User::where('username','=', $this->request['username'])->first();
    if($user)
    {
      if(Hash::check($this->request['password'], $user->password))
      {
        if($user->role == 'admin')
        {
          session()->put('loginId', $user->id);
          session()->put('firstName', $user->first_name);
          session()->put('lastName', $user->last_name);
          session()->put('userName', $user->username);
          return redirect('/dashboard');
        }
        if($user->role == 'cashier'){
          session()->put('loginId', $user->id);
          session()->put('firstName', $user->first_name);
          session()->put('lastName', $user->last_name);
          session()->put('userName', $user->username);
          return redirect('/product-cart');
        }
        if($user->role == 'manager'){
          session()->put('loginId', $user->id);
          session()->put('firstName', $user->first_name);
          session()->put('lastName', $user->last_name);
          session()->put('userName', $user->username);
          return redirect('/dashboard-v2');
        }
      }else{
        return redirect()->back()->withErrors(['error' => 'Incorrect Password, Try again.']);
      }
    }
  
  }

}
