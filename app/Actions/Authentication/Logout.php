<?php

namespace App\Actions\Authentication;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;

class Logout
{

  public function handle(){
    if(Session::has('loginId')){
      Session::pull('loginId');
    }
  }
}
