<?php
namespace App\Actions\BrandAction;

use App\Models\Brand;
use Illuminate\Http\Request;
use function view;


class AllBrand
{
    public function handle(){
      return $data = Brand::all();
    }
}

