<?php
namespace App\Actions\BrandAction;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;

class CreateBrand
{
    public function handle(Request $request)
    {
        //validation
       $validatedInput = $request->validate([
            'name'=>'required|string|unique:brands,name'
       ]);
        $result = Brand::query()->create($validatedInput);
    }
}
