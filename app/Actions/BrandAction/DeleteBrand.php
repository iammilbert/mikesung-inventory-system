<?php
namespace App\Actions\BrandAction;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class DeleteBrand
{
    public function handle($id){

        $item = Brand::findOrFail($id);
        return $item->delete();

    }
}

