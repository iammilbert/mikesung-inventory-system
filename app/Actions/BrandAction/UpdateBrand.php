<?php

namespace App\Actions\BrandAction;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;



class UpdateBrand
{
    public function handle($id, Request $request)
       {

            $validatedData = $request->validate([
                'name' => 'max:225|required|string|unique:brands,name,'. $id,
            ]);

            $brand = Brand::findOrFail($id);
            $brand->update($validatedData);
       }
} 
