<?php
namespace App\Actions\CategoryAction;

use App\Models\Category;
use Illuminate\Http\Request;


class AllCategory
{
    public function handle(){
       return Category::all();
    }
}

