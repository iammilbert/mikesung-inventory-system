<?php

namespace App\Actions\CategoryAction;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;

class CreateCategory
{
    public function handle(Request $request)
    {
        //validation
       $validatedInput = $request->validate([
            'name'=>'required|max:225|string|unique:categories,name'
       ]);
    
        $category = Category::query()->create($validatedInput);
    }
}
