<?php

namespace App\Actions\CategoryAction;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DeleteCategory
{
    public function handle($id){

        $item = Category::findOrFail($id);
        return $item->delete();

    }
}

 