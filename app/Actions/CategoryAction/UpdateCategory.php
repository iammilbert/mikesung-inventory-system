<?php

namespace App\Actions\CategoryAction;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class UpdateCategory
{
    public function handle($categoryId, Request $request)
       {

            $validatedData = $request->validate([
                'name' => 'max:225|required|string|unique:categories,name,'. $categoryId,
            ]);

            $category = Category::findOrFail($categoryId);
            $category->update($validatedData);
       }
} 
 