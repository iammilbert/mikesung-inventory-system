<?php
namespace App\Actions\CompanyActions;

use App\Models\Company;
use Illuminate\Http\Request;


class AllCompanies
{
    public function handle(){
        
        $data = Company::all();
        return $data;
    }
}

