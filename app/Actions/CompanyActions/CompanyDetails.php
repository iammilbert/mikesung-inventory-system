<?php
namespace App\Actions\CompanyActions;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class CompanyDetails
{
    public function handle(Request $request){
        $data = Company::all()->where('id', $request->id);
        return view('company-details', compact('data'));
    }
}

