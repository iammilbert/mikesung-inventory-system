<?php
namespace App\Actions\CompanyActions;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;

class CreateCompany
{
    public function handle(Request $request)
    {
      $validatedInput = $request->validate([
            'company_name' =>'required|string|max:225|unique:companies,company_name',
            'company_address' =>'required|string|max:225',
            'company_email' =>'string|email|max:225|unique:companies,company_email',
            'company_phone'=>'string|max:22|unique:companies,company_phone',
        ]);

        $create = Company::query()->create($validatedInput);
        return $create;

    }
}
