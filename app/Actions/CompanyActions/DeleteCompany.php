<?php
namespace App\Actions\CompanyActions;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class DeleteCompany
{
    public function handle($id){

        $company = Company::findOrFail($id);
        return $company->delete();

    }
}

