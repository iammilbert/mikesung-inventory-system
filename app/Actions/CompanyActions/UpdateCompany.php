<?php
namespace App\Actions\CompanyActions;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UpdateCompany
{
    public function handle($id, Request $request)
    {

      $validatedInput = $request->validate([
            'company_name' =>'sometimes|required|string|max:225|unique:companies,company_name,'. $id,
            'company_address' =>'sometimes|required|string|max:225',
            'company_email' =>'string|email|max:225|unique:companies,company_email,'. $id,
            'company_phone'=>'string|max:225|unique:companies,company_phone,'. $id
      ]);

         $company = Company::findOrFail($id);
         $company->update($validatedInput);
    
    }
}
