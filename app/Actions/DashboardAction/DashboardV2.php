<?php
namespace App\Actions\DashboardAction;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Hash;

class DashboardV2
{
  public function handle(){
    $data = array();
    if(Session::has('loginId')){
      $data = User::where('id', '=', Session::get('longinId'))->first();
    }
    return view('docs.dashboard-v2', compact('data'));
  }
}
