<?php
namespace App\Actions\EmployeeActions;

use App\Models\Employee;
use Illuminate\Support\Facades\Session;

class AllEmployee
{
    public function handle()
    {
      return Employee::all();
    }
}
