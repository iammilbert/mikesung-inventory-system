<?php
namespace App\Actions\EmployeeActions;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DeleteEmployee
{

    public function handle(Request $request)
    {
        $user = Employee::query()->where('id', $request->id)->delete();

        if ($user->delete() === false) {
            Session::flash('fail', 'error occur, try again!');
            return redirect()->back();
        }

        Session::flash('success', 'User Deleted Successfully');
        return redirect()->back();
    }
}
