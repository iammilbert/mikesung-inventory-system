<?php
namespace App\Actions\EmployeeActions;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EmployeeDetails
{

    public function handle(Request $request){

        $data = Employee::all()->where('id', $request->id);

        return view('user-profile', compact('data'));
    }

}
