<?php
namespace App\Actions\EmployeeActions;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;
use Illuminate\Support\Facades\Validator;

class RegisterEmployee 
{
    public function handle(Request $request)
    {
        //validation
        $request->validate([
            'first_name' => 'required|string|max:225',
            'last_name' => 'required|string|max:225',
            'middle_name' => 'string|max:225',
            'email' => 'required|string|email|unique:employees,email|max:225',
            'salary' => 'required|string|max:225',
            'position' => 'required|string|max:225',
            'dob' => 'required|string|max:225',
            'state_of_origin' => 'required|string|max:225',
            'lga' => 'required|string|max:225',
            'permanent_address' => 'string|max:225',
            'residential_address' => 'required|string|max:225',
            'phone' => 'required|string|max:225|unique:employees,phone',
            'employment_date' => 'required|string|max:225',
            'resumption_date' => 'required|string|max:255',
            'employment_status' => 'required|string|max:225'
            
        ]);


        $result = Employee::query()->create($request->all());
        session()->flash('success', 'Employee Registered Successfully.');
        return redirect()->route("employees.index");
    }
}
