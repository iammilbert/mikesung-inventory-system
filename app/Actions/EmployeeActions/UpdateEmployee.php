<?php
namespace App\Actions\UserAction;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class UpdateEmployee
{

    public function handle(Request $request)
    {
        $user = Employee::find($request->id);

        $validator = Validator::make($request->all(), [
            'first_name' => ['sometimes','required', 'string', 'max:225'],
            'last_name' => ['sometimes','required', 'string', 'max:225'],
            'middle_name' => ['string', 'max:225'],
            'email' => 'sometimes|required|string|email|unique:employees,'. $user->id,
            'salary' => ['sometimes','required', 'string', 'max:225'],
            'position' => ['sometimes','required','string', 'max:225'],
            'dob' => ['sometimes','required', 'string', 'max:225'],
            'state_of_origin' => ['sometimes','required','string', 'max:225'],
            'lga' => ['sometimes','required', 'string', 'max:225'],
            'permanent_address' => ['sometimes','required','string', 'max:225'],
            'nationality' => ['sometimes','required','string', 'max:225'],
            'state_of_resident' => ['sometimes','required', 'string', 'max:225'],
            'residential_address' => ['sometimes','required','string', 'max:225'],
            'phone' => 'sometimes|required|string|max:225,'. $user->id,
            'employment_date' => ['sometimes','required','string', 'max:225'],
            'resumption_date' => ['sometimes','required','string', 'max:255'],
            'employment_status' => ['sometimes','required', 'string', 'max:225']
        ]);

        if ($validator->fails()) {
            Session::flash('error', $validator->errors());
            return redirect()->back();
        }

        if ($validator->passes()) {
               $user->update($validator);

            Session::flash('success', 'Employee updated Successfully');
            return redirect()->back();

        }
    }

}
