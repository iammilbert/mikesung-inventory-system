<?php
namespace App\Actions\OrderAction;

use App\Models\Order;
use Illuminate\Http\Request;


class AllOrder
{
    public function handle()
    {
       return $data = Order::where('confirmed_order_status', false)->get();
    }
}

