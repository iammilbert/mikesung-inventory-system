<?php
namespace App\Actions\OrderAction;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CreateOrder
{
    public function handle(Request $request)
    {
        $validatedInput = $request->validate([
            'date_ordered' =>'string|max:225',
            'quantity' =>'required|max:225',
            'unit_cost_price' =>'required|max:225',
            'amount' =>'required|max:225',
            'product_id' =>'required|string|max:225|exists:products,id',
            'ordered_by' =>'required|string|max:225|exists:users,id',
            'company_id' =>'string|max:225|exists:companies,id'
        ]);

        $order = Order::query()->create($validatedInput);
    }




    
}                              
