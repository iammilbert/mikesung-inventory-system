<?php
namespace App\Actions\OrderAction;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class DeleteOrder
{
    public function handle($id)
    {
        $item = Order::findOrFail($id);
      return $item->delete();
    }
}

