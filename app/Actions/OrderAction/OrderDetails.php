<?php
namespace App\Actions\OrderAction;

use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;


class OrderDetails
{
    public function handle(Request $request){

        $request->validate([
            "id" => "required|string|max:25"
        ]);
        
       return $data = Order::all()->where('id', $request->id);
    }
}

