<?php
namespace App\Actions\OrderAction;

use App\Models\Order;
use Illuminate\Http\Request;


class OrderReceived
{
    public function handle()
    {
       return $data = Order::where('confirmed_order_status', true)->where('sales_permission', false)->get();
    }
}

