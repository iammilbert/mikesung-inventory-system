<?php

namespace App\Actions\OrderAction;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class ReceiveOrder
{
    public function handle($id, Request $request)
    {

       $validatedInput = $request->validate([
            'quantity_received' =>'required|max:225',
            'unit_cost_price_received' =>'required|max:225',  
            'total_cost_amount_received'=>'string|max:225',
            'discount_received' =>'max:225',
            'total_after_discount'=>'max:225',
            'expenses' =>'max:225',
            'comment' =>'max:225',
            'confirmed_by' =>'string|max:225|exists:users,id',
            'date_comfirmed' =>'string|max:225',
            'confirmed_order_status'=>'max:225',
            'company_supplier_name'=>'string|max:225',
            'company_supplier_phone'=>'string|max:225'
        ]);

            $order = Order::find($id);

            $order->quantity_received = $request->input('quantity_received');
            $order->unit_cost_price_received = $request->input('unit_cost_price_received');
            $order->total_cost_amount_received = $request->input('total_cost_amount_received');
            $order->discount_received = $request->input('discount_received');
            $order->total_after_discount = $request->input('total_after_discount');
            $order->expenses = $request->input('expenses');
            $order->comment = $request->input('comment');
            $order->date_comfirmed = $request->input('date_comfirmed');
            $order->confirmed_by = $request->input('confirmed_by');
            $order->company_supplier_name = $request->input('company_supplier_name');
            $order->company_supplier_phone = $request->input('company_supplier_phone');
            $order->confirmed_order_status = true;
            
            $order->save();

    }

}
