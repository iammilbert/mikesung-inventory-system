<?php

namespace App\Actions\OrderAction;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class SetPrice
{
    public function handle($id, Request $request)
    {

       $validatedInput = $request->validate([
            'unit_selling_price' =>'required|string|max:225',
            'sales_permission'=> 'max:225',
            'total_amount_expected' =>'required|string|max:225',
            'price_set_by' =>'required|string|max:225|exists:users,id',
            'customer_discount' =>'string|max:225'
        ]);

        $order = Order::query()->find($id);

        $order->unit_selling_price = $request->input('unit_selling_price');
        $order->sales_permission = true;
        $order->total_amount_expected = $request->input('total_amount_expected');
        $order->price_set_by = $request->input('price_set_by');
        $order->customer_discount = $request->input('customer_discount');

    }

}
