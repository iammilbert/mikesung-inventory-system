<?php

namespace App\Actions\OrderAction;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class UpdateOrder
{
    public function handle(Request $request)
    {

       $validatedInput = $request->validate([
            'quantity' =>'required|string|max:225',
            'unit_cost_price' =>'required|string|max:225',
            'amount' =>'required|string|max:225',
            'product_id' =>'required|string|max:225|exists:products,id',
            'company_id' =>'string|max:225|exists:companies,id'
        ]);

        $order = Order::query()->find($request->id);
        $order->update($validatedInput);


    }

}
