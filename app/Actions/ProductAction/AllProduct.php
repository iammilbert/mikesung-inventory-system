<?php
namespace App\Actions\ProductAction;

use App\Models\Product;
use Illuminate\Http\Request;
use function view;


class AllProduct
{
    public function handle(){

        return Product::with(['category', 'measurement'])->get();
    }
}

