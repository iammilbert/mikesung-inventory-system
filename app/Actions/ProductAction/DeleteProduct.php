<?php

namespace App\Actions\ProductAction;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;

class DeleteProduct
{
    public function handle($id){

        $item = Product::findOrFail($id);
        return $item->delete();

    }
}

 