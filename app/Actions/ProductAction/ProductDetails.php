<?php
namespace App\Actions\ProductAction;

use App\Models\Product;
use Illuminate\Http\Request;
use function view;


class ProductDetails
{
    public function handle(Request $request){
        $data = Product::all()->where('id', $request->id);
        return view('product-details', compact('data'));
    }
}

