<?php
namespace App\Actions\ProductAction;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;
use Illuminate\Support\Facades\Validator;

class RegisterProduct
{
    public function handle(Request $request)
    {
        //validation
        $request->validate([
            'name'=>'required|string|unique:products,name',
            'category_id'=>'required|string|exists:categories,id',
            'measurement_id'=>'required|string|exists:measurements,id',
            'unit_qty'=>'required|string'
            
        ]);


        $result = Product::query()->create($request->all());
        session()->flash('msg', 'Product Created Successfully.');
        return redirect()->route("pos.products.index");
    }
}
