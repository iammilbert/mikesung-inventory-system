<?php

namespace App\Actions\ProductAction;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;


class UpdateProduct
{
    public function handle($id, Request $request)
    {
           $validatedInput = $request->validate([
                'name' => 'sometimes|required|string|unique:products,name,'. $id,
                'category_id' =>'sometimes|required|string|max:225|exists:categories,id',
                'measurement_id' =>'sometimes|required|string|max:225|exists:measurements,id',
                'unit_qty'=>'sometimes|required|string|max:225',
        ]);
        $product = Product::find($id)->update($validatedInput);

        return $product;
        }
 } 