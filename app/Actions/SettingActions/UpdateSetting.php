<?php

namespace App\Actions\SettingActions;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UpdateSetting
{

    public function handle(Request $request)
    {
        $user = User::query()->where('id', $request->id);

        $validator = Validator::make($request->all(), [
            'business_name' => 'string|max:255',
            'business_email' => 'string|max:255',
            'business_phone' => 'string|max:255',
            'business_address' => 'string|max:225',
            'receipt_message' => 'string|max:225'
        ]);

        if ($validator->fails()) {
            Session::flash('error', $validator->errors());
            return redirect()->back();
        }

        if ($validator->passes()) {
               $user->update($validator);
            Session::flash('success', 'update Successfully');
            return redirect()->back();

        }
    }

}
