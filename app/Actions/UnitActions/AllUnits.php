<?php
namespace App\Actions\UnitActions;

use App\Models\Measurement;
use Illuminate\Http\Request;


class AllUnits
{
    public function handle(){
       return Measurement::all();
    }
}

