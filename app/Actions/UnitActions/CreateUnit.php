<?php

namespace App\Actions\UnitActions;

use App\Models\Measurement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;

class CreateUnit
{
    public function handle(Request $request)
    {
        //validation
       $validatedInput = $request->validate([
            'name'=>'required|max:225|string|unique:measurements,name'
       ]);
        $unit = Measurement::query()->create($validatedInput);
    }
} 
