<?php

namespace App\Actions\UnitActions;

use App\Models\Measurement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;

class DeleteUnit
{
    public function handle($id){

        $item = Measurement::findOrFail($id);
        return $item->delete();

    }
}


 