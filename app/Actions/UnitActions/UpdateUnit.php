<?php

namespace App\Actions\UnitActions;

use App\Models\Measurement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function redirect;


class UpdateUnit
{
    public function handle($id, Request $request)
    {
        $validatedInput = $request->validate([
            'name' =>'required|max:255|string|unique:measurements,name,'.$id
        ]);

        $unit = Measurement::query()->find($id);

        $unit->update($validatedInput);

    }
}
