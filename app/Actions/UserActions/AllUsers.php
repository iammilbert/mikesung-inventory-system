<?php

namespace App\Actions\UserActions;

use App\Models\User;
use Illuminate\Support\Facades\Session;

class AllUsers
{
    public function handle()
    {
    return User::all();
    }
}
