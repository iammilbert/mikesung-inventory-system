<?php
namespace App\Actions\UserActions;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CreateUser 
{
    public function handle(Request $request)
    {
        $validatedInput =  $request->validate([
            'first_name' => 'required|string|max:225',
            'last_name' => 'required|string|max:225',
            'username' => 'required|string|max:255|unique:users,username',
            'role' => 'required|string|max:255',
            'password' => 'required|string|min:4|max:8|confirmed',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
    }

}
