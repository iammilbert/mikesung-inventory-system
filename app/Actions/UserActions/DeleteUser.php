<?php
namespace App\Actions\UserActions;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class DeleteUser
{

    public function handle(Request $request)
    {
        $user = User::query()->where('id', $request->id)->delete();

        if ($user->delete() === false) {
            Session::flash('fail', 'error occur, try again!');
            return redirect()->back();
        }

        Session::flash('success', 'User Deleted Successfully');
        return redirect()->back();
    }
}
