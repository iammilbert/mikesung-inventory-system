<?php

namespace App\Actions\UserActions;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UpdateUser
{

    public function handle(Request $request)
    {
        $user = User::query()->where('id', $request->id);

        $validatedInput = Validator::make($request->all(), [
            'username' => 'string|max:255,'.$user->id,
            'password' => 'string|max:255',
 
        ]);

            $validatedInput['password'] = Hash::make($request->password);
             $user->update($validatedInput);


    }

}
 