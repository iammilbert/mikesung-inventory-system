<?php

namespace App\Actions\UserActions;

use App\Models\User;
use Illuminate\Http\Request;

class UserDetails
{

    public function handle(Request $request){

        $data = User::all()->where('id', $request->id);

        return view('user-profile', compact('data'));
    }

}
 