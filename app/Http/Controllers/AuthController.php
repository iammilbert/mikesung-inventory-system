<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Actions\Authentication\LoginUser;
use App\Actions\Authentication\Logout;
use App\Actions\DashboardAction\Dashboard;
use App\Actions\DashboardAction\DashboardV2;
use App\Http\Requests\Auth\LoginRequest;
use App\Actions\DashboardAction\ProductCart;
use App\Models\User;
use Hash;

class AuthController extends controller

{

   

    /**
     * Login User
     *
     * @param LoginRequest $request
     * @return \App\Traits\Response
     */
    public function login(LoginRequest $request)
    {
        return (new LoginUser($request->validated()))->execute();
    }

    public function dashboard(Request $request, Dashboard $dashboard)
    {
       return $dashboard->handle($request);
    }

    public function product_cart(Request $request, ProductCart $productCart)
    {
       return $productCart->handle($request);
    }

    public function dashboard_v2(Request $request, DashboardV2 $dashboardV2)
    {
       return $dashboardV2->handle($request);
    }


    public function logout(Request $request, Logout $logout)
    {
       $data = $logout->handle();
       return redirect('/');
    }


}