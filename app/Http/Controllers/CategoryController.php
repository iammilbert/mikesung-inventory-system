<?php
namespace App\Http\Controllers;

use App\Actions\CategoryAction\CreateCategory;
use App\Actions\CategoryAction\UpdateCategory;
use App\Actions\CategoryAction\DeleteCategory;
use App\Actions\CategoryAction\AllCategory;
use App\Models\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(AllCategory $allCategory)
    {
        $categories = $allCategory->handle();
        return view('docs.categories', compact('categories'));
    }


    public function update(Request $request, $categoryId)
    {
        $action = new UpdateCategory();
        $action->handle($categoryId, $request);

        session()->flash('msg', 'Category Updated Successfully.');
        return redirect()->route("pos.categories.index");

    }


    public function destroy($id)
    {
        $action = new DeleteCategory;
        $action->handle($id);

        session()->flash('success', 'Category Deleted Successfully.');
        return redirect()->route("pos.categories.index");

    }


    public function store(Request $request, CreateCategory $createCategory)
    {
        $category = $createCategory->handle($request);
        session()->flash('msg', 'Category Created Successfully.');
        return redirect()->route("pos.categories.index");
    }



}
