<?php
namespace App\Http\Controllers;

use App\Actions\CompanyActions\CreateCompany;
use App\Actions\CompanyActions\UpdateCompany;
use App\Actions\CompanyActions\DeleteCompany;
use App\Actions\CompanyActions\AllCompanies;
use App\Models\Company;

use Illuminate\Http\Request;

class CompanyController extends Controller
{

    public function index(AllCompanies $allCompanies)
    {
        $companies = $allCompanies->handle();
        return view('docs.companies', compact('companies'));
    }


    public function update(Request $request, $id)
    {
        $action = new UpdateCompany();
        $action->handle($id, $request);

        session()->flash('msg', 'Update Successfully.');
        return redirect()->route("pos.companies.index");

    }


    public function store(Request $request, CreateCompany $CreateCompany)
    {
        $company = $CreateCompany->handle($request);
        session()->flash('msg', 'Registered Successfully.');
        return redirect()->route("pos.companies.index");
    }


    public function destroy($id)
    {
        $action = new DeleteCompany;
        $action->handle($id);

        session()->flash('msg', 'Deleted Successfully.');
        return redirect()->route("pos.companies.index");

    }


    
    public function show($id)
    {


    }

}
