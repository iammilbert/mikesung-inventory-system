<?php
namespace App\Http\Controllers;

use App\Actions\CustomerAction\CreateCustomer;
use App\Actions\CustomerAction\CustomerDetails;
use App\Actions\CustomerAction\RegisteredCustomers;
use App\Actions\CustomerAction\DeleteCustomer;
use App\Actions\CustomerAction\UpdateCustomer;

use Illuminate\Http\Request;

class CustomersController extends Controller
{

    public function index(RegisteredCustomers $registeredCustomers)
    {
        $all = $registeredCustomers->handle();
        return $all;
    }


    public function update(Request $request, UpdateCustomer $updateCustomer)
    {
        $patch = $updateCustomer->handle($request);
        return $patch;
    }


    public function store(Request $request, CreateCustomer $createCustomer)
    {
        $create = $createCustomer->handle($request);
        return $create;
    }


    public function delete(Request $request, DeleteCustomer $deleteCustomer)
    {
        $delete = $deleteCustomer->handle($request);
        return $delete;
    }

}
