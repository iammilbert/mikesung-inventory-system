<?php
namespace App\Http\Controllers;

use App\Actions\DebtAction\CreateDebt;
use App\Actions\DebtAction\UpdateDebt;
use App\Actions\DebtAction\RegisteredDebt;
use App\Actions\DebtAction\DeleteDebt;
use App\Actions\DebtAction\DebtDetails;

use Illuminate\Http\Request;

class DebtController extends Controller
{

    public function index(RegisteredDebt $registeredDebt)
    {
        $all = $registeredDebt->handle();
        return $all;
    }


    public function update(Request $request, UpdateDebt $updateDebt)
    {
        $patch = $updateDebt->handle($request);
        return $patch;
    }


    public function store(Request $request, CreateDebt $createDebt)
    {
        $create = $createDebt->handle($request);
        return $create;
    }


    public function delete(Request $request, DeleteCustomer $deleteCustomer)
    {
        $delete = $deleteCustomer->handle($request);
        return $delete;
    }

}
