<?php
namespace App\Http\Controllers;

use App\Models\Employee;

use Illuminate\Http\Request;
use App\Actions\EmployeeActions\RegisterEmployee;
use App\Actions\EmployeeActions\AllEmployee;


class EmployeeController extends Controller
{

    public function index(AllEmployee $allEmployee)
    {
        $employees = $allEmployee->handle();
        return view('docs.employees', compact('employees'));
    }

    public function store(Request $request, RegisterEmployee $registerEmployee)
    { 
        $employee = $registerEmployee->handle($request);
        return $employee;
    }


}
