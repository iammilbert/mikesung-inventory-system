<?php
namespace App\Http\Controllers;

use App\Actions\UnitActions\CreateUnit;
use App\Actions\UnitActions\UpdateUnit;
use App\Actions\UnitActions\DeleteUnit;
use App\Actions\UnitActions\AllUnits;

use Illuminate\Http\Request;

class MeasurementController extends Controller
{

    public function index(AllUnits $allUnits)
    {
        $units = $allUnits->handle();
        return view('docs.units', compact('units'));
    }

    
    public function update(Request $request, $id)
    {
        $action = new UpdateUnit();
        $action->handle($id, $request);

        session()->flash('msg', 'Measurement Unit Updated Successfully.');
        return redirect()->route("pos.units.index");

    }

    public function store(Request $request, CreateUnit $createUnit)
    {
        $unit = $createUnit->handle($request);
        session()->flash('msg', 'Measurement Unit Created Successfully.');
        return redirect()->route("pos.units.index");
    }


    
    public function destroy($id)
    {
        $action = new DeleteUnit;
        $action->handle($id);

        session()->flash('success', 'Measurement unit Deleted Successfully.');
        return redirect()->route("pos.units.index");

    }
}
