<?php
namespace App\Http\Controllers;

use App\Actions\OrderAction\CreateOrder;
use App\Actions\OrderAction\OrderReceived;
use App\Actions\OrderAction\ReceiveOrder;
use App\Actions\OrderAction\UpdateOrder;
use App\Actions\OrderAction\DeleteOrder;
use App\Actions\OrderAction\SetPrice;
use App\Actions\OrderAction\AllOrder;
use App\Actions\ProductAction\AllProduct;
use App\Actions\CompanyActions\AllCompanies;
use App\Actions\userActions\AllUsers;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index(AllOrder $allOrder, AllProduct $allProduct, AllCompanies $allCompanies, AllUsers $allUsers)
    {

        $data['products'] = $allProduct->handle();
        $data['companies'] = $allCompanies->handle();
        $data['users'] = $allUsers->handle();
        $data['orders'] = $allOrder->handle();

        return view('docs.orders', $data);
    }


    public function received_order(OrderReceived $orderReceived, AllProduct $allProduct, AllCompanies $allCompanies, AllUsers $allUsers)
    {

        $order['products'] = $allProduct->handle();
        $order['companies'] = $allCompanies->handle();
        $order['users'] = $allUsers->handle();
        $order['received_orders'] = $orderReceived->handle();

        return view('docs.order-received', $order);
    }


    public function update(Request $request, UpdateOrder $updateOrder)
    {
        $patch = $updateOrder->handle($request);
        session()->flash('msg', 'Order Updated Successfully.');
        return redirect()->route("pos.orders.index");
    }


    public function receive(Request $request, $id)
    {
        $action = new ReceiveOrder();
        $action->handle($id, $request);
        
        session()->flash('msg', 'Order Confirmed/Received Successfully.');
        return redirect()->route("pos.orders.index");
    }

    public function set_price(Request $request, $id)
    {
        $action = new SetPrice();
        $action->handle($id, $request);

        session()->flash('msg', 'Price Set Successfully.');
        return redirect()->route("pos.orders.received");

    }

    public function return_order(Request $request, SetPrice $setPrice, $id)
    {
        $patch = $setPrice->handle($request);
        session()->flash('msg', 'Order Confirmed/Received Successfully.');
        return redirect()->route("pos.orders.index");
    }


    public function store(Request $request, CreateOrder $createOrder)
    {
        $order = $createOrder->handle($request);
        session()->flash('msg', 'Order Created Successfully.');
        return redirect()->route("pos.orders.index");
    }




    public function destroy($id)
    {
        $action = new DeleteOrder;
        $action->handle($id);

        session()->flash('msg', 'Order Deleted Successfully.');
        return redirect()->route("pos.orders.index");

    }



    public function show(Request $request, $id)
    {

    }

}
