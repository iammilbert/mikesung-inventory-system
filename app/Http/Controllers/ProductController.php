<?php
namespace App\Http\Controllers;

use App\Actions\BrandAction\AllBrand;
use App\Actions\CategoryAction\AllCategory;
use App\Actions\UnitActions\AllUnits;
use App\Actions\ProductAction\AllProduct;
use App\Actions\ProductAction\RegisterProduct;
use App\Actions\ProductAction\UpdateProduct;
use App\Actions\ProductAction\DeleteProduct;

use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(AllProduct $allProduct, AllBrand $allBrand, AllCategory $allCategory, AllUnits $allUnits)
    {
        $data['units'] = $allUnits->handle();
        $data['categories'] = $allCategory->handle();
        $data['products'] = $allProduct->handle();

        return view('docs.products', $data);
    }


    public function update(Request $request, $id)
    {
        $action = new UpdateProduct();
        $action->handle($id, $request);

        session()->flash('msg', 'Product Updated Successfully.');
        return redirect()->route("pos.products.index");

    }


    public function store(Request $request, RegisterProduct $registerProduct)
    {
        $product = $registerProduct->handle($request);
        return $product; 
    }


    public function destroy($id)
    {
        $action = new DeleteProduct;
        $action->handle($id);

        session()->flash('success', 'Product Deleted Successfully.');
        return redirect()->route("pos.products.index");

    }


    public function details(Request $request, ProductDetails $productDetails)
    {
        $details = $productDetails->handle($request);
        return $details;
    }

}
