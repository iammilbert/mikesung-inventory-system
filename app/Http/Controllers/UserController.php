<?php
namespace App\Http\Controllers;

use App\Actions\UserActions\CreateUser;
use App\Actions\UserActions\DeleteUser;
use App\Actions\UserActions\UpdateUser;
use App\Actions\UserActions\AllUsers;
use App\Actions\EmployeeActions\AllEmployee;
use App\Actions\UserActions\UserDetails;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index(AllUsers $allUsers, AllEmployee $allEmployee)
    {
        $data['cashiers'] = $allUsers->handle();
        $data['employees'] = $allEmployee->handle();

        return view('docs.cashiers', $data);
    }


    public function details(Request $request, UserDetails $userDetails)
    {
        $details = $userDetails->handle($request);
        return $details;
    }

    public function update(Request $request, UpdateUser $updateUser)
    {
        $patch = $updateUser->handle($request);
        session()->flash('success', 'Cashier updated Successfully');
        return redirect()->route("cashiers.index");
    }


    public function store(Request $request, CreateUser $createUser)
    {
        $cashiers = $createUser->handle($request);
        session()->flash('success', 'Cashier Created Successfully');
        return redirect()->route("cashiers.index");
    }

    public function delete(Request $request, $id)
    {
        return (new DeleteUser(['id' => $id]))->execute();
    }

}








