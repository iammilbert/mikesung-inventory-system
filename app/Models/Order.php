<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UUIDHelper;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    use HasFactory, UUIDHelper;

    protected $table = 'orders';

    protected $guarded = [
        'id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'ordered_by');
    }

    public function userconfim()
    {
        return $this->belongsTo(User::class, 'confirmed_by');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

}