<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UUIDHelper;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model
{
    use HasFactory, UUIDHelper;

    protected $table = 'users';

    protected $guarded = [
        'id'
    ];

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}