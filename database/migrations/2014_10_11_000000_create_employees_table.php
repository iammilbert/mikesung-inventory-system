<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('first_name')->index();
            $table->string('last_name')->index();
            $table->string('middle_name')->nullable();
            $table->string('position')->index();
            $table->string('dob')->index();
            $table->string('state_of_origin')->index();
            $table->string('lga')->index();
            $table->string('permanent_address')->index();
            $table->string('residential_address')->index();
            $table->string('email')->nullable()->index();
            $table->string('phone')->index();
            $table->double('salary')->index();
            $table->string('employment_date')->index();
            $table->text('resumption_date')->index();
            $table->string('employment_status')->default('active');
            $table->timestamps();
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
