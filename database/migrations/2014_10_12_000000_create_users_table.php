<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('first_name')->index();
            $table->string('last_name')->index();
            $table->string('token')->nullable()->index();
            $table->string('role')->index();
            $table->string('username')->unique()->index();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });   
        
        

        \Illuminate\Support\Facades\DB::table('users')
        ->insert([
            'id' => \Illuminate\Support\Str::uuid(),
            'first_name' => 'Next-Wave',
            'last_name' => 'Inventory',
            'username' => 'admin@example.com',
            'password' => bcrypt('password'),
            'role' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
