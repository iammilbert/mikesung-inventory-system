<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->date('date')->index();
            $table->string('expense_type');
            $table->double('amount');
            $table->foreignUuid('reg_by')->constrained('users')->cascadeOnDelete();
            $table->foreignUuid('incured_by')->constrained('users')->cascadeOnDelete();
            $table->foreignUuid('approved_by')->constrained('users')->cascadeOnDelete();
            $table->string('comment')->nullable();
            $table->string('receipt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
};
