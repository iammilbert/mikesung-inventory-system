<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('business_name')->nullable();
            $table->string('business_email')->nullable();
            $table->string('business_phone')->nullable();
            $table->string('business_address')->nullable();
            $table->string('receipt_message')->nullable();
            $table->timestamps();
        });


        \Illuminate\Support\Facades\DB::table('settings')
        ->insert([
            'id' => \Illuminate\Support\Str::uuid(),
            'business_name' => 'Mikesung Next-Wave',
            'business_phone' => '08137950284',
            'business_email' => 'mikesungnextwave@gmail.com',
            'receipt_message' => 'We Strive to Serve You Better',
            'business_address' => 'No.1 Yakubu way, Karji Junction, Kaduna',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
};
