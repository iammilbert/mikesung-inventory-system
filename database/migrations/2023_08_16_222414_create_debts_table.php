<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('description')->nullabe;
            $table->foreignUuid('customer_id')->constrained('customers')->cascadeOnDelete();
            $table->foreignUuid('product_id')->constrained('products')->cascadeOnDelete();
            $table->foreignUuid('approved_by')->constrained('users')->cascadeOnDelete();
            $table->double('initial_amount')->default(0);
            $table->double('total_amount_paid')->nullable()->default(0);
            $table->double('first_installment')->nullable()->default(0);
            $table->string('first_installment_date')->nullable();
            $table->string('second_installment')->nullable()->default(0);
            $table->string('second_installment_date')->nullable();
            $table->string('third_installment')->nullable()->default(0);
            $table->string('third_installment_date')->nullable();
            $table->string('debt_type')->index(); // sales, general
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debts');
    }
};
