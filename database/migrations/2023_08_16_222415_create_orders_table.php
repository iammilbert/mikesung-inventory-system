<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('product_id')->constrained('products')->cascadeOnDelete();
            $table->foreignUuid('ordered_by')->constrained('users')->cascadeOnDelete();
            $table->foreignUuid('company_id')->constrained('companies')->cascadeOnDelete();
            $table->string('date_ordered');
            $table->double('unit_cost_price')->default(0);    
            $table->double('amount')->default(0);                                                                                                                                                                    
            $table->integer('quantity')->default(0);
            $table->integer('quantity_received')->nullable()->default(0);
            $table->double('expenses')->default(0)->nullable();
            $table->string('comment')->default("NIL")->nullable();
            $table->double('unit_cost_price_received')->default(0)->nullable();
            $table->double('total_cost_amount_received')->default(0)->nullable();
            $table->double('discount_received')->default(0)->nullable();
            $table->boolean('confirmed_order_status')->default(false);
            $table->double('total_after_discount')->default(0)->nullable();
            $table->string('date_comfirmed')->nullable();
            $table->foreignUuid('confirmed_by')->nullable()->constrained('users')->cascadeOnDelete();

            $table->double('unit_selling_price')->default(0)->nullable();
            $table->string('company_supplier_name')->nullable(); 
            $table->string('company_supplier_phone')->nullable(); 
            $table->double('total_amount_expected')->default(0)->nullable();
            $table->foreignUuid('price_set_by')->nullable()->constrained('users')->cascadeOnDelete();
            $table->double('customer_discount')->default(0)->nullable();
            $table->boolean('sales_permission')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * 
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
