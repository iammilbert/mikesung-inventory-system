<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('customer_id')->constrained('customers')->cascadeOnDelete();
            $table->foreignUuid('product_id')->constrained('products')->cascadeOnDelete();
            $table->foreignUuid('debt_id')->constrained('debts')->cascadeOnDelete();
            $table->foreignUuid('sold_by')->constrained('users')->cascadeOnDelete();
            $table->string('customer_name')->nullable();
            $table->string('customer_phone')->nullable();
            $table->double('discount')->default(0);
            $table->string('total');
            $table->string('method_of_payment')->nullable()->default('cash');       
            $table->decimal('amount', 11, 2)->nullable()->default(0.0);
            $table->decimal('debt_balance', 11, 2)->nullable()->default(0.0 );
            $table->string('on_hold')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
};
