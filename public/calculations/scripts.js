        // Get input elements and result spans
        var quantity = document.getElementById('quantity');
        var unit_cost_price = document.getElementById('unit_cost_price');
        var amount = document.getElementById('amount');

		 // Add input event listeners
		 quantity.addEventListener('input', updateResults);
		 unit_cost_price.addEventListener('input', updateResults);

        // Function to calculate total cost Price
        function calculate() {

		// Get the values from the input fields and convert them to numbers
		const qty = parseFloat(quantity.value) || 0;
		const cost = parseFloat(unit_cost_price.value) || 0;

            // Update multiplication result
            const result = qty * cost;
            totalAmount.textContent = amount;

		// Update the result element
		amount.textContent = result;

        }

        // Initial the caculation
        calculate();




