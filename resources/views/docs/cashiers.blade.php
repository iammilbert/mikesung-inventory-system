@include ('docs/header')


<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{url('auths/dashboard')}}" class="site_title"><i class="fa fa-paw"></i> <span class="font-weight-bold">MIS Inventory </span></a>
                </div>

                <div class="clearfix"></div>
                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        <li class="nav child_menu">                                    
                                    <li class="font-weight-bold" style="font-size:25px;"><a href="{{url('auths/dashboard')}}">Dashboard</a></li>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Sales <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('sales/cart')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Make Sales</a></li>
                                    <li><a href="{{url('sales/return')}}" style="font-size:16px;"><i class="fa fa-reply"></i>Return Sales</a></li>
                                    <li><a href="{{url('sales/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Held Receipt</a></li>
                                    <li><a href="{{url('sales/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Today Sales</a></li>
                                    <li><a href="{{url('sales/index')}}" style="font-size:16px;"><i class="fa fa-book"></i> All Sales</a></li>
                                </ul>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Inventory <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('inventories/order-received')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Order Received</a></li>
                                    <li><a href="{{url('inventories/sellable')}}" style="font-size:16px;"><i class="fa fa-dollar"></i>Sellable Products</a></li>
                                    <li><a href="{{url('inventories/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Expired Products</a></li>
                                    <li><a href="{{url('inventories/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Damaged Products</a></li>
                                </ul>
                            </li>


                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-truck"></i> Order <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.orders.index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Place Order</a></li>
                                    <li><a href="{{route('pos.orders.received')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Order Received</a></li>
                                    <li><a href="{{route('pos.orders.placed')}}" style="font-size:16px;"><i class="fa fa-truck"></i> All Orders Placed</a></li>
                                </ul>
                            </li>



                        <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">REGISTRATIONS</b></li>

                        <li><a href="{{ route('pos.products.index') }}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Products</a></li>
                        <li><a href="{{route('pos.suppliers.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Supplier</a></li>
                        <li><a href="{{url('expenses/index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Expenses</a></li>
                        <li><a href="{{route('pos.debts.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Debt</a></li>

                        <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-product-hunt"></i> Product Features<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.categories.index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Product Categories</a></li>
                            <li><a href="{{route('pos.units.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Measurement Units</a></li>
                           
                                </ul>
                            </li>


                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-sitemap "></i> General Staff <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('employees/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Employee</a></li>
                                    <li><a href="{{url('cashiers/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Cashier</a></li>
                                </ul>
                            </li>
                          </li>
                          <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Debts <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a href="{{url('pos.debts.customer')}}" style="font-size:16px;"><i class="fa fa-book"></i> Customer Debts</a></li>
                                    <li><a href="{{url('pos.debts.staff')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Staff Debts</a></li>
                                </ul>
                            </li>


                            <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">OFFICIAL</b></li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-building "></i> Company <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Company Documents</a></li>
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Tasks/Revenue</a></li>
                                    <li><a href="{{url('companies/setting')}}" style="font-size:16px;"><i class="fa fa-setting mt-3"></i>Company Settings</a></li>
                                </ul>
                            </li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Reports <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('reports/order')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Order</a></li>
                                    <li><a href="{{url('reports/cashier')}}" style="font-size:16px;"><i class="fa fa-book"></i> Cashier</a></li>
                                    <li><a href="{{url('reports/employee')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Employee</a></li>
                                    <li><a href="{{url('reports/product')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Products</a></li>
                                    <li><a href="{{url('reports/sales')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Sales</a></li>
                                    <li><a href="{{url('reports/Invoice')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Invoices</a></li>
                                </ul>
                            </li>

                            
                           
                    </div>

                </div>
                <!-- /sidebar menu -->
                
                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>
 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 40px; padding-right: 40px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle lg" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="../images/img.jpg" alt="">
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="javascript:;"> Profile </a>
                        <a class="dropdown-item"  href="javascript:;">
                          <span>Settings</span>
                        </a>
                    <form action="{{ route('auths.logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item"  href="{{ route('auths.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                    </form>
                    </div>
                  </li>
  
                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../images/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->


    <!-- page content -->
    <div class="right_col" role="main">
          <div class="mt-3">
            <div class="row">
              <div class="col-md-12">
                  <div class="title_right mt-5">
                      <div class="pull-right">
                        <div class="form-group">
                          <button class="btn btn-success btn-sm" style="font-size:20px;" data-toggle="modal" data-target="#modal-primary"><i class="fa fa-plus"></i> New Cashier</button>
                        </div>
                      </div>
                    </div>
                <div class="x_panel">
    
                    <h2 class="font-weight-bold">CASHIERS TABLE</h2>
                
                  <div class="x_content">
                   <!-- /.card-header -->
                  <div class="card-body">
                  @if(session('success'))
                          <div class="alert alert-success">
                              {{ session('success') }}
                          </div>
                   @endif

                    
                  @if ($errors->any())
                    <div class="error-list">
                        <h6 class="text-danger font-weight-bold">Validation Errors:</h6>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                   @endif

                    <table id="example1" class="table font-size-14">
                      <thead>
                          <tr>
                          <th>S/NO</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>USERNAME</th>
                            <th>ROLE</th>                           
                            <th>ACTIONS</th> 
                          </tr>
                      </thead>
                      <tbody>
                      @foreach ($cashiers as $cashier)
                    <tr>
                    <td>{{ $loop->iteration}}</td>
                        <td>{{ $cashier->first_name }}</td>
                        <td>{{ $cashier->last_name }}</td>
                        <td>{{ $cashier->username }}</td>
                        <td><small class="badge badge-primary">{{ $cashier->role }}</small></td>
                        <td>
                          <a class="btn btn-danger text-white" data-target="#editModal{{ $cashier->id }}" data-toggle="modal" id="modal-edit" title="Edit"><i class="fa fa-edit"></i></a>
                          <a class="btn btn-primary text-white"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach       
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
            </div> 
          </div>
        </div> 
      </div> 


      <div class="modal fade" id="modal-primary" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h5 class="modal-title font-weight-bold" id="staticBackdropLabel" style="color:white;">Cashier Registration</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="car card-primary">
            <!-- form start -->
            <form class="form" method="POST" action="{{route('cashiers.create-cashier')}}">
                @csrf
                <div class="card-body">
            <div class="row">
                <div class="col-md-12" >
                <div class="input-group mb-4">
                          <label for="first_name" class="input-group-text">First Name</label>
                          <input type="text" class="form-control" name="first_name"  value="{{ old('first_name') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="last_name" class="input-group-text">Last Name</label>
                          <input type="text" class="form-control" name="last_name"  value="{{ old('last_name') }}">
                    </div> 

                    <div class="input-group mb-4">
                          <label for="username" class="input-group-text">Username</label>
                          <input type="text" class="form-control" name="username"  value="{{ old('username') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="password" class="input-group-text">Password</label>
                          <input type="passord" class="form-control" name="password"  value="{{ old('password') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="password_confirmation" class="input-group-text">Confirm Passord</label>
                          <input type="text" class="form-control" name="password_confirmation">
                    </div> 
                        
                    <div class="input-group mb-3">
                  <label for="role" class="input-group-text">Role</label>
                      <select class="form-control" name="role" value="{{ old('role') }}" >
                          <option disabled selected>Select role</option>
                          <option value="admin">admin</option> 
                          <option value="cashier">cashier</option> 
                          <option value="manager">manager</option>
                          <option value="accountant">accountant</option>
                      </select> 
                      
                 </div>
                     </div>                 
                    </div>
                  </div>
                </div>
             <!-- /.card-body -->
               <div class="modal-footer justify-content-between bg-primary">
                  
                  <button type="button" class="btn btn-outline-light btn-danger" data-dismiss="modal">Cancel</button>

                  <button type="submit" class="btn btn-outline-light btn-primary" name="submit">Submit</button>
              </div>
            </div>
              </form>
          
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>



      <div class="modal fade" id="editModal{{ $cashier->id }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
        <div class="modal-content">
    <form action="{{ route('cashiers.update', $cashier->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="modal-header">
            <h5 class="modal-title" id="editModalLabel">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="name">Username</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $cashier->username }}">
            </div>
            <!-- Add other input fields as needed -->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
    </form>
</div>

        </div>
        <!-- /.modal-dialog -->
      </div>

    </div>
</div>
</div>
</div>

<script>
  function populateForm(id, name){
    document.getElementById('category_id').value = id
    document.getElementById('name').innerHTML = name
  }
</script>

<script src="../myassets/js/jquery.dataTables.min.js"></script>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>



<script src="../plugins/select2/js/select2.full.min.js"></script>

<!-- DataTables  & Plugins -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../plugins/jszip/jszip.min.js"></script>
<script src="../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  })

 
</script>

</body>
</html>
