<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>MIS Inventory</title>

    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{url('auths/dashboard')}}" class="site_title"><i class="fa fa-paw"></i> <span class="font-weight-bold">MIS Inventory </span></a>
                </div>

                <div class="clearfix"></div>
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        <li class="nav child_menu">                                    
                                    <li class="font-weight-bold" style="font-size:25px;"><a href="{{url('auths/dashboard')}}">Dashboard</a></li>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Sales <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('sales/cart')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Make Sales</a></li>
                                    <li><a href="{{url('sales/return')}}" style="font-size:16px;"><i class="fa fa-reply"></i>Return Sales</a></li>
                                    <li><a href="{{url('sales/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Held Receipt</a></li>
                                    <li><a href="{{url('sales/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Today Sales</a></li>
                                    <li><a href="{{url('sales/index')}}" style="font-size:16px;"><i class="fa fa-book"></i> All Sales</a></li>
                                </ul>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Inventory <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('inventories/order-received')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Order Received</a></li>
                                    <li><a href="{{url('inventories/sellable')}}" style="font-size:16px;"><i class="fa fa-dollar"></i>Sellable Products</a></li>
                                    <li><a href="{{url('inventories/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Expired Products</a></li>
                                    <li><a href="{{url('inventories/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Damaged Products</a></li>
                                </ul>
                            </li>


                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-truck"></i> Order <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.orders.index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Place Order</a></li>
                                    <li><a href="{{route('pos.orders.received')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Order Received</a></li>
                                    <li><a href="{{route('pos.orders.placed')}}" style="font-size:16px;"><i class="fa fa-truck"></i> All Orders Placed</a></li>
                                </ul>
                            </li>



                        <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">REGISTRATIONS</b></li>

                        <li><a href="{{ route('pos.products.index') }}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Products</a></li>
                        <li><a href="{{route('pos.companies.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Companies</a></li>
                        <li><a href="{{url('expenses/index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Expenses</a></li>
                        <li><a href="{{route('pos.debts.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Debt</a></li>

                        <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-product-hunt"></i> Product Features<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.categories.index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Product Categories</a></li>
                            <li><a href="{{route('pos.units.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Measurement Units</a></li>
                           
                                </ul>
                            </li>


                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-sitemap "></i> General Staff <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('employees/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Employee</a></li>
                                    <li><a href="{{url('cashiers/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Cashier</a></li>
                                </ul>
                            </li>
                          </li>
                          <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Debts <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a href="{{url('pos.debts.customer')}}" style="font-size:16px;"><i class="fa fa-book"></i> Customer Debts</a></li>
                                    <li><a href="{{url('pos.debts.staff')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Staff Debts</a></li>
                                </ul>
                            </li>


                            <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">OFFICIAL</b></li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-building "></i> Company <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Company Documents</a></li>
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Tasks/Revenue</a></li>
                                    <li><a href="{{url('companies/setting')}}" style="font-size:16px;"><i class="fa fa-setting mt-3"></i>Company Settings</a></li>
                                </ul>
                            </li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Reports <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('reports/order')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Order</a></li>
                                    <li><a href="{{url('reports/cashier')}}" style="font-size:16px;"><i class="fa fa-book"></i> Cashier</a></li>
                                    <li><a href="{{url('reports/employee')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Employee</a></li>
                                    <li><a href="{{url('reports/product')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Products</a></li>
                                    <li><a href="{{url('reports/sales')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Sales</a></li>
                                    <li><a href="{{url('reports/Invoice')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Invoices</a></li>
                                </ul>
                            </li>

                            
                           
                    </div>

                </div>
                <!-- /sidebar menu -->


                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        
 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 40px; padding-right: 40px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle lg" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="../images/img.jpg" alt="">
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="javascript:;"> Profile </a>
                        <a class="dropdown-item"  href="javascript:;">
                          <span>Settings</span>
                        </a>
                    <form action="{{ route('auths.logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item"  href="{{ route('auths.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                    </form>
                    </div>
                  </li>
  
                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../images/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->

           <!-- page content -->
      <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="card">
          <div class="row">
            <div class="col-md-12 col-sm-12 ">
            <div class="col-md-2 col-sm-4  tile_stats_count card-body">
              <span class="count_top"><i class="fa fa-train"></i> AVAILABLE PRODUCTS</span>
              <div class="count font-weight-bold" style="font-size:18px;">2500</div>
              <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count card-body">
              <span class="count_top"><i class="fa fa-truck"></i> ORDER RAISED</span>
              <div class="count font-weight-bold" style="font-size:18px;">2500</div>
              <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>

            <div class="col-md-2 col-sm-4  tile_stats_count card-body">
              <span class="count_top"><i class="fa fa-logistics"></i> DAMAGED </span>
              <div class="count font-weight-bold" style="font-size:18px;">2500</div>
              <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>

            <div class="col-md-2 col-sm-4  tile_stats_count card-body">
              <span class="count_top"><i class="fa fa-logistics"></i> EXPIRED</span>
              <div class="count font-weight-bold" style="font-size:18px;">2500</div>
              <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count card-body">
              <span class="count_top"><i class="fa fa-clock-o"></i> TODAY SALES</span>
              <div class="count">123.50</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> Yesterday Sales</span>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count card-body">
              <span class="count_top"><i class="fa fa-user"></i> COMPANIES</span>
              <div class="count">7,325</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
          </div>
        </div>
      </div>
          <!-- /top tiles -->
          <div class="clearfix"></div>

         <!-- top tiles -->
         <div class="card mt-3">
          <div class="row">
            <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                  <div class="x_content">
                <!-- /.card-header -->
                <div class="card-body">
                @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                @endif

                  
                @if ($errors->any())
                  <div class="error-list">
                      <h6 class="text-danger font-weight-bold">Validation Errors:</h6>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li class="text-danger">{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                @endif

                  <table id="example1" class="table font-size-14 table-striped">
                    <thead>
                        <tr>
                          <th>PRODUCT NAME</th>
                          <th>QTY</th>
                          <th>TOTAL</th>
                          <th>CASHIER</th> 
                          <th>PAYMENT</th> 
                          <th>STATUS</th> 
                        </tr>
                    </thead>
                    <tbody>
                  <tr>
                      <td><span class="text-uppercase font-weight-bold table-bordered">pom</td>
                      <td>{{ session('loginId') }}</td>
                      <td>{{ session('firstName') }}</td>
                      <td>{{ session('lastName') }}</td>
                      <td><small class="badge badge-success font-weight-bold">{{ session('userName') }}</small></td>
                      
                      <td>
                      <a class="btn btn-primary text-white"><i class="fa fa-check"></i></a>
                     
                      </td>
                  </tr>
     
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div> 
              </div>
          </div>
        </div>
      </div>
          <!-- /top tiles -->
    </div>
</div>







        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Developed by <a href="https://michael.darlcloudsoft.com">Michael Gabriel</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

</body>
</html>
