@include ('docs/header')

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{url('auths/dashboard')}}" class="site_title"><i class="fa fa-paw"></i> <span class="font-weight-bold">MIS Inventory </span></a>
                </div>

                <div class="clearfix"></div>
                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        <li class="nav child_menu">                                    
                                    <li class="font-weight-bold" style="font-size:25px;"><a href="{{url('auths/dashboard')}}">Dashboard</a></li>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Sales <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('sales/cart')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Make Sales</a></li>
                                    <li><a href="{{url('sales/return')}}" style="font-size:16px;"><i class="fa fa-reply"></i>Return Sales</a></li>
                                    <li><a href="{{url('sales/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Held Receipt</a></li>
                                    <li><a href="{{url('sales/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Today Sales</a></li>
                                    <li><a href="{{url('sales/index')}}" style="font-size:16px;"><i class="fa fa-book"></i> All Sales</a></li>
                                </ul>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Inventory <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('inventories/order-received')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Order Received</a></li>
                                    <li><a href="{{url('inventories/sellable')}}" style="font-size:16px;"><i class="fa fa-dollar"></i>Sellable Products</a></li>
                                    <li><a href="{{url('inventories/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Expired Products</a></li>
                                    <li><a href="{{url('inventories/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Damaged Products</a></li>
                                </ul>
                            </li>


                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-truck"></i> Order <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.orders.index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Place Order</a></li>
                                    <li><a href="{{route('pos.orders.received')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Order Received</a></li>
                                    <li><a href="{{route('pos.orders.placed')}}" style="font-size:16px;"><i class="fa fa-truck"></i> All Orders Placed</a></li>
                                </ul>
                            </li>



                        <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">REGISTRATIONS</b></li>

                        <li><a href="{{ route('pos.products.index') }}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Products</a></li>
                        <li><a href="{{route('pos.companies.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Companies</a></li>
                        <li><a href="{{url('expenses/index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Expenses</a></li>
                        <li><a href="{{route('pos.debts.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Debt</a></li>

                        <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-product-hunt"></i> Product Features<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.categories.index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Product Categories</a></li>
                            <li><a href="{{route('pos.units.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Measurement Units</a></li>
                           
                                </ul>
                            </li>


                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-sitemap "></i> General Staff <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('employees/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Employee</a></li>
                                    <li><a href="{{url('cashiers/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Cashier</a></li>
                                </ul>
                            </li>
                          </li>
                          <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Debts <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a href="{{url('pos.debts.customer')}}" style="font-size:16px;"><i class="fa fa-book"></i> Customer Debts</a></li>
                                    <li><a href="{{url('pos.debts.staff')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Staff Debts</a></li>
                                </ul>
                            </li>


                            <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">OFFICIAL</b></li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-building "></i> Company <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Company Documents</a></li>
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Tasks/Revenue</a></li>
                                    <li><a href="{{url('companies/setting')}}" style="font-size:16px;"><i class="fa fa-setting mt-3"></i>Company Settings</a></li>
                                </ul>
                            </li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Reports <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('reports/order')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Order</a></li>
                                    <li><a href="{{url('reports/cashier')}}" style="font-size:16px;"><i class="fa fa-book"></i> Cashier</a></li>
                                    <li><a href="{{url('reports/employee')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Employee</a></li>
                                    <li><a href="{{url('reports/product')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Products</a></li>
                                    <li><a href="{{url('reports/sales')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Sales</a></li>
                                    <li><a href="{{url('reports/Invoice')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Invoices</a></li>
                                </ul>
                            </li>

                            
                           
                    </div>

                </div>
                <!-- /sidebar menu -->

                
                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>


 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 40px; padding-right: 40px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle lg" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="../images/img.jpg" alt="">
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="javascript:;"> Profile </a>
                        <a class="dropdown-item"  href="javascript:;">
                          <span>Settings</span>
                        </a>
                    <form action="{{ route('auths.logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item"  href="{{ route('auths.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                    </form>
                    </div>
                  </li>
  
                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../images/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->

    <!-- page content -->
    <div class="right_col" role="main">
          <div class="mt-3">
            <div class="row">
              <div class="col-md-12">
                  <div class="title_right mt-5">
                      <div class="pull-right">
                        <div class="form-group">
                          <button class="btn btn-success btn-sm" style="font-size:20px;" data-toggle="modal" data-target="#modal-primary"><i class="fa fa-plus"></i> New Employee</button>
                        </div>
                      </div>
                    </div>
                <div class="x_panel">
    
                    <h2 class="font-weight-bold">Employees Table</h2>
                
                  <div class="x_content">
                   <!-- /.card-header -->
                  <div class="card-body">
                  @if(session('success'))
                          <div class="alert alert-success">
                              {{ session('success') }}
                          </div>
                   @endif

                    
                  @if ($errors->any())
                    <div class="error-list">
                        <h6 class="text-danger font-weight-bold">Validation Errors:</h6>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                   @endif

                    <table id="example1" class="table font-size-14">
                      <thead>
                          <tr>
                            <th>NAME</th>
                            <th>DOB</th>
                            <th>SALARY</th>
                            <th>POSITION</th> 
                            <th>STATUS</th> 
                            
                            <th>Actions</th> 
                          </tr>
                      </thead>
                      <tbody>
                      @foreach ($employees as $employee)
                    <tr>
                        <td><span class="text-uppercase font-weight-bold">{{ $employee->last_name }}, </span> {{ $employee->first_name }} {{ $employee->middle_name }}</td>
                        <td>{{ $employee->dob }}</td>
                        <td>{{ $employee->salary }}</td>
                        <td>{{ $employee->position }}</td>
                        <td><small class="badge badge-success font-weight-bold">{{ $employee->employment_status }}</small></td>
                        
                        <td>
                        <a class="btn btn-primary text-white"><i class="fa fa-trash"></i></a>
                        <a onclick="populateForm('{{$employee->id}}', '{{$employee->first_name}}')" class="btn btn-danger text-white" data-target="#modal-edit-category" data-toggle="modal" id="modal-edit" title="Edit"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach       
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
            </div> 
          </div>
        </div> 
      </div> 


      <div class="modal fade" id="modal-primary" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h5 class="modal-title font-weight-bold" id="staticBackdropLabel" style="color:white;">Employee Registration</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="car card-primary">
            <!-- form start -->
            <form class="form" method="POST" action="{{route('employees.create-employee')}}">
                @csrf
                <div class="card-body">
            <div class="row">
                <div class="col-md-6" > 
                <div class="input-group mb-4">
                          <label for="first_name" class="input-group-text">First name</label>
                          <input type="text" class="form-control" name="first_name"  value="{{ old('first_name') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="last_name" class="input-group-text">Surname</label>
                          <input type="text" class="form-control" name="last_name"  value="{{ old('last_name') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="middle_name" class="input-group-text">Middle name</label>
                          <input type="text" class="form-control" name="middle_name" value="{{ old('middle_name') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="position" class="input-group-text">Position</label>
                          <input type="text" class="form-control" name="position" value="{{ old('position') }}">
                    </div> 
                      <div class="input-group mb-4">
                          <label for="dob" class="input-group-text">Date of Birth</label>
                          <input type="date" class="form-control" name="dob" value="{{ old('dob') }}">
                        </div>
                        <div class="input-group mb-4">
                          <label for="phone" class="input-group-text">Phone No.</label>
                          <input type="text" class="form-control" name="phone"  placeholder="e.g. 08137950284"  value="{{ old('phone') }}">
                    </div>
                    
                    <div class="input-group mb-4">
                          <label for="" class="input-group-text">Start Date</label>
                          <input type="date" class="form-control" name="resumption_date" id="resumption_date" value="{{ old('resumption_date') }}">
                    </div> 

                    <div class="input-group mb-4">
                          <input type="hidden" class="form-control" value="Active" name="employment_status" >
                    </div> 
                </div>

                <div class="col-md-6" > 
                    <div class="input-group mb-4">
                          <label for="state_of_origin" class="input-group-text">State of Origin</label>
                          <input type="text" class="form-control" name="state_of_origin"  value="{{ old('state_of_origin') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="lga" class="input-group-text">LGA</label>
                          <input type="text" class="form-control" name="lga"  value="{{ old('lga') }}">
                    </div> 
                    <div class="input-group mb-4">
                          <label for="permanent_address" class="input-group-text">Permanent Address</label>
                          <input type="text" class="form-control" name="permanent_address"  value="{{ old('permanent_address') }}">
                    </div> 

                    <div class="input-group mb-4">
                          <label for="dob" class="input-group-text">Email Address</label>
                          <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div> 

                    <div class="input-group mb-4">
                          <label for="dob" class="input-group-text">Residential Address</label>
                          <input type="text" class="form-control" name="residential_address" value="{{ old('residential_address') }}">
                    </div> 

                    <div class="input-group mb-4">
                          <label for="dob" class="input-group-text">Salary</label>
                          <input type="number" class="form-control" name="salary" value="{{ old('salary') }}">
                    </div> 

                    <div class="input-group mb-4">
                          <label for="dob" class="input-group-text">Employment Date</label>
                          <input type="date" class="form-control" name="employment_date">
                    </div> 
 
                        
                     </div>                 
                    </div>
                  </div>
                </div>
             <!-- /.card-body -->
               <div class="modal-footer justify-content-between bg-primary">
                  
                  <button type="button" class="btn btn-outline-light btn-danger" data-dismiss="modal">Cancel</button>

                  <button type="submit" class="btn btn-outline-light btn-primary" name="submit">Submit</button>
              </div>
            </div>
              </form>
          
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>



      <div class="modal fade" id="modal-edit-category" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h5 class="modal-title font-weight-bold" id="staticBackdropLabel" style="color:white;">Product Category</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="car card-primary">
                         <!-- form start -->
              <form class="form" method="POST" action="create-category">
                @csrf
                <div class="card-body">

                  <div class="row">
                    <div class="input-group mb-4">
                          <input type="text" class="form-control" name="name" id="name" placeholder="Category name" value="{{old('name')}}">
                          <input type="text" class="form-control" name="category_id" id="id" placeholder="Category name" >
                          <span class="text-danger" style="font-size: 11px;">@error('name') {{$message}} @enderror</span>
                    </div>                  
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
               <div class="modal-footer justify-content-between bg-primary">
                  
                  <button type="button" class="btn btn-outline-light btn-danger" data-dismiss="modal">Cancel</button>

                  <button type="submit" class="btn btn-outline-light btn-primary" name="submit">Submit</button>
              </div>
            </div>
              </form>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

    </div>
</div>
</div>
</div>

<script>
    // Listen for click events on the delete buttons
    document.addEventListener('click', function (event) {
        if (event.target.classList.contains('delete-record')) {
            const recordId = event.target.getAttribute('data-record');

            // Display the SweetAlert confirmation dialog
            Swal.fire({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // If the user confirms, submit the form for deletion
                    const form = document.querySelector(`form[action="${event.target.form.action}"]`);
                    form.submit();
                }
            });
        }
    });
</script>

<script src="../myassets/js/jquery.dataTables.min.js"></script>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>



<script src="../plugins/select2/js/select2.full.min.js"></script>

<!-- DataTables  & Plugins -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../plugins/jszip/jszip.min.js"></script>
<script src="../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  })

 
</script>

</body>
</html>
