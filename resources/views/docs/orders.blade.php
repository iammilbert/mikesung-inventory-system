@include ('docs/header')


<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{url('auths/dashboard')}}" class="site_title"><i class="fa fa-paw"></i> <span class="font-weight-bold">MIS Inventory </span></a>
                </div>

                <div class="clearfix"></div>
                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        <li class="nav child_menu">                                    
                                    <li class="font-weight-bold" style="font-size:25px;"><a href="{{url('auths/dashboard')}}" class="text-success font-weight-bold mb-3">Dashboard</a></li>
                            </li>
                            <li><a href="{{url('sales/cart')}}" style="font-size:16px;"><i class="fa fa-desktop mb-2"></i>Make Sales</a></li>

                            
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Inventory <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.orders.received')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Order Received</a></li>
                                    <li><a href="{{url('inventories/sellable')}}" style="font-size:16px;"><i class="fa fa-dollar"></i>Sellable Products</a></li>
                                    <li><a href="{{url('inventories/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Expired Products</a></li>
                                    <li><a href="{{url('inventories/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Damaged Products</a></li>
                                </ul>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart"></i> Sales Records<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    
                                    <li><a href="{{url('sales/return')}}" style="font-size:16px;"><i class="fa fa-reply"></i>Return Sales</a></li>
                                    <li><a href="{{url('sales/held-receipt')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Held Receipt</a></li>
                                    <li><a href="{{url('sales/today-sales')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Today Sales</a></li>
                                    <li><a href="{{url('sales/index')}}" style="font-size:16px;"><i class="fa fa-book"></i> All Sales</a></li>
                                </ul>
                            </li>

                            <li><a href="{{route('pos.orders.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-2"></i>Place Order</a></li>



                        <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">REGISTRATIONS</b></li>

                        <li><a href="{{ route('pos.products.index') }}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Products</a></li>
                        <li><a href="{{route('pos.companies.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Companies</a></li>
                        <li><a href="{{url('expenses/index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Expenses</a></li>
                        <li><a href="{{route('pos.debts.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Debt</a></li>

                        <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-product-hunt"></i> Product Features<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('pos.categories.index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Product Categories</a></li>
                            <li><a href="{{route('pos.units.index')}}" style="font-size:16px;"><i class="fa fa-plus mb-1"></i>Measurement Units</a></li>
                           
                                </ul>
                            </li>


                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-sitemap "></i> General Staff <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('employees/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Employee</a></li>
                                    <li><a href="{{url('cashiers/index')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Cashier</a></li>
                                </ul>
                            </li>
                          </li>
                          <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Debts <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a href="{{url('pos.debts.customer')}}" style="font-size:16px;"><i class="fa fa-book"></i> Customer Debts</a></li>
                                    <li><a href="{{url('pos.debts.staff')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Staff Debts</a></li>
                                </ul>
                            </li>


                            <li class="pt-4"><b style="font-size:18px;" class="text-warning font-weight-bold ml-3 mt-5 text-uppercase">OFFICIAL</b></li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-building "></i> Company <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Company Documents</a></li>
                                    <li><a href="{{url('companies/revenue')}}" style="font-size:16px;"><i class="fa fa-book"></i>Tasks/Revenue</a></li>
                                    <li><a href="{{url('companies/setting')}}" style="font-size:16px;"><i class="fa fa-setting mt-3"></i>Company Settings</a></li>
                                </ul>
                            </li>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-book "></i> Reports <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('reports/order')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Order</a></li>
                                    <li><a href="{{url('reports/cashier')}}" style="font-size:16px;"><i class="fa fa-book"></i> Cashier</a></li>
                                    <li><a href="{{url('reports/employee')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Employee</a></li>
                                    <li><a href="{{url('reports/product')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Products</a></li>
                                    <li><a href="{{url('reports/sales')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Sales</a></li>
                                    <li><a href="{{url('reports/Invoice')}}" style="font-size:16px;"><i class="fa fa-truck"></i>Invoices</a></li>
                                </ul>
                            </li>

                            
                           
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>


 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 40px; padding-right: 40px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle lg" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="../images/img.jpg" alt="">
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="javascript:;"> Profile </a>
                        <a class="dropdown-item"  href="javascript:;">
                          <span>Settings</span>
                        </a>
                    <form action="{{ route('auths.logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item"  href="{{ route('auths.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                    </form>
                    </div>
                  </li>
  
                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../images/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->

    <!-- page content -->
    <div class="right_col" role="main">
          <div class="mt-3">
            <div class="row">
              <div class="col-md-12">
                  <div class="title_right mt-5">
                      <div class="pull-right">
                        <div class="form-group">
                          <button class="btn btn-success btn-sm" style="font-size:20px;" data-toggle="modal" data-target="#modal-primary"><i class="fa fa-plus"></i> New ORDER</button>
                        </div>
                      </div>
                    </div>
                <div class="x_panel">
    
                    <h2 class="font-weight-bold">ORDER PRODUCTS</h2>
                
                  <div class="x_content">
                   <!-- /.card-header -->
                <div class="card-body">
                  @if(session('success'))
                          <div class="alert alert-success">
                              {{ session('success') }}
                          </div>
                   @endif

                    
                  @if ($errors->any())
                    <div class="error-list">
                        <h6 class="text-danger font-weight-bold">Validation Errors:</h6>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                   @endif

                    <table id="example1" class="table font-size-14">
                    <thead>
                          <tr>
                            <th>PRODUCT NAME</th>
                            <th>QUANTITY</th>
                            <th>UNIT COST PRICE</th>
                            <th>TOTOAL AMOUNT</th>
                            <th>ORDER BY</th>
                            <th>COMPANY NAME</th>
                            <th>Action</th> 
                          </tr>
                      </thead>
                      <tbody>
                      @foreach ($orders as $order)
                    <tr>
                        <td>{{ $order->product->name }}</td>
                        <td>{{ $order->quantity }}</td>
                        <td>{{ $order->unit_cost_price }}</td>
                        <td>{{ $order->amount }}</td>
                        <td>{{ $order->user->first_name }}</td>
                        <td>{{ $order->company->company_name }}</td>
                        
                        <td>
                        <form action="{{route('pos.orders.delete', $order->id)}}" method="POST">
                        <a class='btn btn-success text-white'  data-toggle="modal" data-target="#modal-success-product-edit{{$order->id}}"><i class='fa fa-edit'></i></a> 
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-danger delete-record" data-record="{{ $order->id }}"><i class="fa fa-trash"></i></button>
                            <a class="btn btn-primary text-white btn-sm receive-order"  data-toggle="modal" data-target="#modal-success-order-receive{{$order->id}}">Receive</a> 

                        </form>
                        </td>
                    </tr>
                    @endforeach                
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
            </div> 
          </div>
        </div> 
      </div> 


      <div class="modal fade" id="modal-primary" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h5 class="modal-title font-weight-bold" id="staticBackdropLabel" style="color:white;">Placing Order</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="car card-primary">
                         <!-- form start -->
            <form class="form" id="example1" method="post" action="{{ route('pos.orders.store')}}">
                @csrf
                <div class="card-body">
                  <div class="row">
                  <label> Company </label>
                  <div class="input-group">
                   
                      <select class="form-control" name="company_id" id="company_id">
                          <option disabled selected>Choose</option>
                          @forelse ($companies as $company)
                              <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                            @empty
                              <option value="Anonymouse"></option>
                          @endforelse
                      </select>
                      </div>

                      <label> Products </label>
                      <div class="input-group">
                      <select class="form-control" name="product_id" id="product_id">
                          <option disabled selected>Select</option>
                          @forelse ($products as $product)
                              <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @empty
                              <option value="Anonymouse"></option>
                          @endforelse
                      </select>
                      </div>


                      <label class="font-weight-bolder"> Quantity <b style="font-size:12px;"> (i.e No. of Cartons, Pieces, Pack etc.)</b></label>
                     <div class="input-group mb-3">
                          <input type="number" class="form-control" name="quantity" placeholder="e.g. 10 cartons">
                    </div>

                    <label> Unit Cost Price </label>
                    <div class="input-group mb-3">
                          <input type="number" class="form-control" name="unit_cost_price" id="unit_cost_price">
                          <input type="hidden" class="form-control" name="ordered_by" id="ordered_by" value="{{ session('loginId') }}">
                          <input type="hidden" class="form-control" name="confirmed_order_status" id="confirmed_order_status">
                    </div>

                    <label> Total Cost Price </label>
                    <div class="input-group mb-3">
                          <input type="text" class="form-control" name="amount" id="amount">
                          <input type="hidden" class="form-control" name="date_ordered" id="date_ordered" value="{{ date('Y-m-d') }}" >
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
               <div class="modal-footer justify-content-between bg-primary">
                  
                  <button type="button" class="btn btn-outline-light btn-danger" data-dismiss="modal">Cancel</button>

                  <button type="submit" class="btn btn-outline-light btn-primary" name="submit">Submit</button>
              </div>
            </div>
              </form>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

@foreach($orders as $order)
      <div class="modal fade" id='modal-success-product-edit{{$order->id}}' data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h5 class="modal-title font-weight-bold" id="staticBackdropLabel" style="color:white;">UPDATING PRODUCT ORDERED</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="car card-primary">
                         <!-- form start -->
      <form class="form" method="post" action="{{ route('pos.orders.update', $order->id)}}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <label class="font-weight-bolder">Product Name</label>
                        <div class="form-group">
                            <select class="form-control select2" name="product_id" id="product_id" style="width:100%">
                                <option selected value="{{ $order->product->id }}" selected>{{ $order->product->name }}</option>
                                @forelse ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                  @empty
                                    <option value="Anonymouse"></option>
                                @endforelse
                            </select>
                        </div>


                    <label class="font-weight-bolder">Company Name</label>
                    <div class="form-group">
                        <select class="form-control select2" name="company_id" id="company_id" style="width:100%">
                            <option selected disabled value="{{$order->company->company_name}}" selected>{{ $order->company->company_name}}</option>
                            @forelse ($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                              @empty
                                <option value="Anonymouse"></option>
                            @endforelse
                        </select>
                    </div>

                    <label class="font-weight-bolder">Unit Quantity</label>
                    <div class="form-group">              
                          <input type="text" class="form-control" name="quantity" id="quantity" value="{{ $order->quantity }}">
                    </div>


                    <label class="font-weight-bolder"> Unit Cost Price </label>
                    <div class="input-group mb-3">
                          <input type="text" class="form-control" name="unit_cost_price" id="unit_cost_price"  value="{{ $order->unit_cost_price }}">
                    </div>

                    <label class="font-weight-bolder"> Total Cost Price </label>
                    <div class="input-group mb-3">
                          <input type="text" class="form-control" name="amount" id="amount" value="{{ $order->amount }}">
                        
                    </div>
              </div>
        <!-- /.card-body -->
                <div class="modal-footer justify-content-between bg-primary">
                  
                  <button type="button" class="btn btn-outline-light btn-danger" data-dismiss="modal">Cancel</button>

                  <button type="submit" class="btn btn-outline-light btn-primary" name="submit">update</button>
              </div>
              </form>
 
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@endforeach





@foreach($orders as $order)
      <div class="modal fade" id='modal-success-order-receive{{$order->id}}' data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h5 class="modal-title font-weight-bold" id="staticBackdropLabel" style="color:white;">Receiving {{$order->product->name}} Ordered</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="text-center"> 
                <h1 class="font-weight-bold text-success">{{$order->product->name}}</h1>
                <h6><span class="text-danger">{{ $order->product->unit_qty}} Pieces, Per {{ $order->product->measurement->name}}</span>.</h6>
              </div>
       
            <div class="car card-primary">
                         <!-- form start -->
      <form class="form" method="post" action="{{ route('pos.orders.receive', $order->id)}}">
                @csrf
                @method('PUT')
                <div class="card-body">              

                    <label class="font-weight-bolder">Quantity Received <b style="font-size:12px;">(i.e. Qty In Cartons, Pieces, Pack, Dozen, etc. base on Product Unit of Measurement)</b></label>
                    <div class="form-group">              
                          <input type="number" class="form-control" name="quantity_received" id="quantity_received" value="{{ old('quantity_received') }}">
                    </div>
            
                      <input type="hidden" class="form-control" name="confirmed_by" id="confirmed_by" value="{{ session('loginId') }}">

                      <input type="hidden" class="form-control" name="date_comfirmed" id="date_comfirmed" value="{{ date('Y-m-d') }}">

                                            
                    <label class="font-weight-bolder"> Cost Price Per unit </label>
                    <div class="input-group mb-2">
                          <input type="number" class="form-control" name="unit_cost_price_received" id="unit_cost_price_received"  value="{{ old('unit_cost_price_received') }}">
                    </div>

                    <label class="font-weight-bolder"> Total Cost Price Received </label>
                    <div class="input-group mb-2">
                          <input type="number" class="form-control" name="total_cost_amount_received" id="total_cost_amount_received" value="{{ old('total_cost_amount_received') }}">
                    </div>
                    
                    <label class="font-weight-bolder"> Discount Received ? </label>
                    <div class="input-group mb-2">
                          <input type="number" class="form-control" name="discount_received" id="discount_received" value="{{ old('discount_received') }}">
                    </div>
                                        
                    <label class="font-weight-bolder"> Total Cost After Discount</label>
                    <div class="input-group mb-2">
                          <input type="number" class="form-control" name="total_after_discount" id="total_after_discount" value="{{ old('total_after_discount') }}">
                    </div>

                    <label class="font-weight-bolder"> Expenses</label>
                    <div class="input-group mb-2">
                          <input type="number" class="form-control" name="expenses" id="expenses" value="{{ old('expenses') }}">
                    </div>

                    <label class="font-weight-bolder"> Supplier Name</label>
                    <div class="input-group mb-2">
                          <input type="text" class="form-control" name="company_supplier_name" id="company_supplier_name" value="{{ old('company_supplier_name') }}">
                    </div>

                    <label class="font-weight-bolder"> Supplier Mobile</label>
                    <div class="input-group mb-2">
                          <input type="phone" class="form-control" name="company_supplier_phone" id="company_supplier_phone" value="{{ old('company_supplier_phone') }}">
                    </div>

                    <label class="font-weight-bolder"><b> comment</b></label>
                    <div class="input-group">
                      <textarea name="comment" class="form-control text-black font-weight-bold italic" colspan="3" placeholder="any comment ?"></textarea>
                        
                    </div>

              </div>
        <!-- /.card-body -->
                <div class="modal-footer justify-content-between bg-primary">
                  
                  <button type="button" class="btn btn-outline-light btn-danger" data-dismiss="modal">Cancel</button>

                  <button type="submit" class="btn btn-outline-light btn-primary" name="submit">update</button>
              </div>
              </form>
 
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@endforeach

    </div>
</div>
</div>
</div>





<!-- Include Sweet Alert scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>

@if(session('msg'))
        Swal.fire({
            title: 'Success!',
            text: '{{ session('msg') }}',
            icon: 'success',
            timer: 3000
        });
    @endif


    @if($errors->any())
        @foreach($errors->all() as $error)
            Swal.fire({
                title: 'FAILED',
                text: '{{ $error }}',
                icon: 'error',
                timer: 10000
            });
        @endforeach
    @endif
</script>

<script>
    // Listen for click events on the delete buttons
    document.addEventListener('click', function (event) {
        if (event.target.classList.contains('delete-record')) {
            const recordId = event.target.getAttribute('data-record');

            // Display the SweetAlert confirmation dialog
            Swal.fire({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // If the user confirms, submit the form for deletion
                    const form = document.querySelector(`form[action="${event.target.form.action}"]`);
                    form.submit();
                }
            });
        }
    });
</script>

<!-- <script>
    var dt = new Date();
    document.getElementById('date-time').innerHTML=dt;
</script> -->

<script src="../myassets/js/jquery.dataTables.min.js"></script>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

<!-- Autocalculation -->
<!-- Custom scripts -->
<script >
          // Get input elements and result spans
          var quantity = document.getElementById('quantity');
        var unit_cost_price = document.getElementById('unit_cost_price');
        var amount = document.getElementById('amount');

		 // Add input event listeners
		 quantity.addEventListener('input', calculate);
		 unit_cost_price.addEventListener('input', calculate);

        // Function to calculate total cost Price
        function calculate() {

		// Get the values from the input fields and convert them to numbers
		const qty = parseFloat(quantity.value) || 0;
		const cost = parseFloat(unit_cost_price.value) || 0;

            // Update multiplication result
            const result = qty * cost;
                // Update the result elements
            amount.textContent = result;

        }

        // Initial the caculation
        calculate();

</script>

<script src="../plugins/select2/js/select2.full.min.js"></script>

<!-- DataTables  & Plugins -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../plugins/jszip/jszip.min.js"></script>
<script src="../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  })

 
</script>

</body>
</html>
