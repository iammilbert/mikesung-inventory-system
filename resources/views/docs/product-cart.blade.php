<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>MIS Inventory</title>

    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span class="font-weight-bold">MIS Inventory </span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="../images/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome, {{$sessionId}}</span>
                        <h2>John Doe</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        <li class="nav child_menu">                                    
                                    <li class="font-weight-bold" style="font-size:25px;"><a href="{{url('docs/index')}}">Dashboard</a></li>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-product-hunt"></i> Sales <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('docs.products')}}" style="font-size:16px;"><i class="fa fa-desktop"></i>Make Sales</a></li>
                                    <li><a href="{{url('docs/products')}}" style="font-size:16px;"><i class="fa fa-reply"></i>Return Sales</a></li>
                                    <li><a href="{{url('docs/register-product')}}" style="font-size:16px;"><i class="fa fa-exclamation-circle"></i> Held Receipt</a></li>
                                    <li><a href="{{url('docs/products')}}" style="font-size:16px;"><i class="fa fa-product-hunt"></i>Today Sales</a></li>
                                    <li><a href="{{url('docs/register-product')}}" style="font-size:16px;"><i class="fa fa-book"></i> All Sales</a></li>
                                </ul>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-product-hunt"></i> Products <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a href="{{url('products')}}" style="font-size:16px;"><i class="fa fa-plus"></i> Add New Product</a></li>
                                    <li><a href="{{url('products/expired')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Expired Products</a></li>
                                    <li><a href="{{url('docs/damage-products')}}" style="font-size:16px;"><i class="fa fa-book"></i>Damaged Products</a></li>
                                </ul>
                            </li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart "></i> Order <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('docs/place-order')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Place Order</a></li>
                                    <li><a href="{{url('docs/orders')}}" style="font-size:16px;"><i class="fa fa-book"></i> Confirm Order</a></li>
                                    <li><a href="{{url('docs/received-order')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Order Received</a></li>
                                </ul>
                            </li>

                            <li><a href="{{url('docs/categories')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Product Categories</a></li>
                            <li><a href="{{url('docs/unit')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Measurement Units</a></li>

                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-sitemap "></i> Expenses <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('docs/place-order')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Add New Expenses</a></li>
                                    <li><a href="{{url('docs/orders')}}" style="font-size:16px;"><i class="fa fa-book"></i> All Expenses</a></li>
                                </ul>
                            </li>
                            <li><a style="font-size:18px;">Reports</a>
                            <li class="font-weight-bold" style="font-size:18px;"><a><i class="fa fa-shopping-cart "></i> Order <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('docs/place-order')}}" style="font-size:16px;"><i class="fa fa-plus"></i>Place Order</a></li>
                                    <li><a href="{{url('docs/orders')}}" style="font-size:16px;"><i class="fa fa-book"></i> Confirm Order</a></li>
                                    <li><a href="{{url('docs/received-order')}}" style="font-size:16px;"><i class="fa fa-truck"></i> Order Received</a></li>
                                </ul>
                            </li>
                           
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

    <!-- page content -->
    <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Plain Page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5   form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Plain Page</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      Add content to the page ...
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Developed by <a href="https://michael.darlcloudsoft.com">Michael Gabriel</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

</body>
</html>
