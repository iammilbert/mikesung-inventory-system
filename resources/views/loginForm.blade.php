<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ne-wave Inventory">
    <meta name="author" content="Next-Wave">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form|Log in</title>

    <!-- alert -->
    <script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap + LeadMark main styles -->
    <link rel="stylesheet" href="assets/css/leadmark.css">
    <link rel="stylesheet" href="assets/css/adminlte.min.css">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

                   


<body class="login">
<div class="login-background-image">
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form method="POST" action="{{route('auths.login')}}">
                    @csrf
                   
                    <h1 class="text-center text-info text-cyan pb-2" style="list-style: none; font-family: Exotc350 Bd BT; font-size: 35px;">Login Account</h5>
                    <div class="form-group">
                        <input type="username" name="username" class="form-control tex-white btn-outline-primary rounded-0 font-weight-bold" placeholder="Username"  value="{{ old('username') }}"/>
                        @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                    </div>
                    <div class="form-group pt-3">
                        <input type="password" class="form-control text-primary rounded-0 font-weight-bold" name="password" placeholder="Password" value="{{ old('password') }}"/>
                        @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>

                    <div class="p-4">
                        <button type="submit" class="btn btn-flat btn-block btn-outline-primary font-weight-bolder text-white">Log in</button>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator text-primary">


                        <div class="clearfix"></div>
                        <br />
                        <div class="text-center">
                            <i class="fa fa-paw"></i>
                        </div>
                        <div>
                            <h1>{{ config('app.name') }}</h1>
                            <p>©2016 All Rights Reserved. Mikesung Nextwave Ltd. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>


<!-- Include Sweet Alert scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    @if($errors->any())
        @foreach($errors->all() as $error)
            Swal.fire({
                title: 'FAILED',
                text: '{{ $error }}',
                icon: 'error',
                timer: 10000
            });
        @endforeach
    @endif
</script>



<script src="https://formspree.io/js/formbutton-v1.min.js" defer></script>
<script>
    window.formbutton =
        window.formbutton ||
        function() {
            (formbutton.q = formbutton.q || []).push(arguments);
        };
    formbutton("create", {
        action:"https://formspree.io/f/xeqwgkvr",
        title: "Contact Us",
        fields: [
            {
                type: "text",
                label: "Name:",
                name: "name",
            },
            {
                type: "email",
                label: "Email:",
                name: "email",
            },
            {
                type: "text",
                label: "Address:",
                name: "address1",
            },
            {
                type: "text",
                label: "&nbsp;",
                name: "address2",
            },
            {
                type: "textarea",
                label: "Message:",
                name: "message",
            },
            {
                type: "checkbox",
                label: "Please send me your monthly newsletter",
                name: "_optin",
            },
            { type: "submit", value: "Send" }
        ],
        styles: {
            fontFamily: "Montserrat",
            modal: {
                border: "1px solid #6D6875",
                boxShadow: "6px 6px 0 #6D6875",
                borderRadius: "0",
            },
            title: {
                padding: "24px 24px 0px 24px",
                background: "rgba(0,0,0,0)",
                color: "#2e2a37",
                fontFamily: "Marcellus SC",
                fontSize: "2em",
            },
            body: {
                padding: "16px 24px 24px",
            },
            field: {
                display: "flex",
            },
            submitField: {
                justifyContent: "flex-end",
            },
            label: {
                width: "40%",
            },
            checkboxLabel: {
                width: "auto",
            },
            input: {
                borderRight: "1px solid rgba(0,0,0,0.1)",
                borderBottom: "1px solid rgba(0,0,0,0.1)",
                borderRadius: "0px"
            },
            button: {
                background: "text-primary",
                fill: "skyblue",
                border: "1px solid #6D6875",
                boxShadow: "3px 3px 0px #6D6875"
            },
            closeButton: {
                textShadow: "0 0 0 #2e2a37",
            }
        },
        initiallyVisible: false
    });
</script>

<!-- Back to Top -->
<!--  <a href="#" class="btn btn-primary btn-square back-to-top"><i class="fa fa-arrow-up"></i></a> -->


<!-- core  -->
<script src="assets/vendors/jquery/jquery-3.4.1.js"></script>
<script src="assets/vendors/bootstrap/bootstrap.bundle.js"></script>

<!-- bootstrap 3 affix -->
<script src="assets/vendors/bootstrap/bootstrap.affix.js"></script>

<!-- Isotope -->
<script src="assets/vendors/isotope/isotope.pkgd.js"></script>

<!-- LeadMark js -->
<script src="assets/js/leadmark.js"></script>
<script src="assets/js/adminlte.min.js"></script>

</body>
</html>


