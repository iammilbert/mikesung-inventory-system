<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\MeasurementController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DebtController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ExpensesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('loginForm');
});

Route::as('auths.')->group(function () {
    Route::post('login', [ AuthController::class, 'login'])->name('login');
    Route::post('logout', [ AuthController::class, 'logout'])->name('logout');
    Route:: get('/dashboard', [ AuthController::class, 'dashboard'])->name('/dashboard');
});


Route::as('pos.products.')->prefix('products')->group(function() {
    Route::get('/', [ ProductController::class, 'index'])->name('index');
    Route::post('/', [ ProductController::class, 'store'])->name('store');
    // Route::get('/{id}', [ ProductController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ ProductController::class, 'update'])->name('update');
    Route::delete('/{id}', [ ProductController::class, 'destroy'])->name('delete');
});

Route::as('pos.orders.')->prefix('orders')->group(function() {
    Route::get('/', [ OrderController::class, 'index'])->name('index');
    Route::post('/', [ OrderController::class, 'store'])->name('store');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ OrderController::class, 'update'])->name('update');
    Route::delete('/{id}', [ OrderController::class, 'destroy'])->name('delete');
    Route::get('/received', [ OrderController::class, 'received_order'])->name('received');
    Route::put('receive/{id}', [ OrderController::class, 'receive'])->name('receive');
    Route::put('available/{id}', [ OrderController::class, 'make_available'])->name('available');
    Route::put('return/{id}', [ OrderController::class, 'return_order'])->name('return');
    Route::put('price/{id}', [ OrderController::class, 'set_price'])->name('price');
    Route::get('/placed', [ OrderController::class, 'order_placed'])->name('placed');

});

Route::as('pos.categories.')->prefix('categories')->group(function() {
    Route::get('/', [ CategoryController::class, 'index'])->name('index');
    Route::post('/', [ CategoryController::class, 'store'])->name('store');
    Route::get('/{category}', [ CategoryController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{category}', [ CategoryController::class, 'update'])->name('update');
    Route::delete('/{id}', [ CategoryController::class, 'destroy'])->name('delete');
});

Route::as('pos.units.')->prefix('units')->group(function() {
    Route::get('/', [ MeasurementController::class, 'index'])->name('index');
    Route::post('/', [ MeasurementController::class, 'store'])->name('store');
    Route::get('/{id}', [ MeasurementController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ MeasurementController::class, 'update'])->name('update');
    Route::delete('/{id}', [ MeasurementController::class, 'destroy'])->name('delete');
});

Route::as('pos.brands.')->prefix('brands')->group(function() {
    Route::get('/', [ BrandController::class, 'index'])->name('index');
    Route::post('/', [ BrandController::class, 'store'])->name('store');
    Route::get('/{id}', [ BrandController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ BrandController::class, 'update'])->name('update');
    Route::delete('/{id}', [ BrandController::class, 'destroy'])->name('delete');
});


Route::as('pos.companies.')->prefix('companies')->group(function() {
    Route::get('/', [ CompanyController::class, 'index'])->name('index');
    Route::post('/', [ CompanyController::class, 'store'])->name('store');
    // Route::get('/{id}', [ CompanyController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ CompanyController::class, 'update'])->name('update');
    Route::delete('/{id}', [ CompanyController::class, 'destroy'])->name('delete');
});

Route::as('pos.debts.')->prefix('debts')->group(function() {
    Route::get('/', [ DebtController::class, 'index'])->name('index');
    Route::post('/', [ DebtController::class, 'store'])->name('store');
    Route::get('/{id}', [ DebtController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ DebtController::class, 'update'])->name('update');
    Route::delete('/{id}', [ DebtController::class, 'destroy'])->name('delete');
});


Route::as('pos.employees.')->prefix('employees')->group(function() {
    Route::get('/', [ EmployeeController::class, 'index'])->name('index');
    Route::post('/', [ EmployeeController::class, 'store'])->name('store');
    // Route::get('/{id}', [ EmployeeController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ EmployeeController::class, 'update'])->name('update');
    Route::delete('/{id}', [ EmployeeController::class, 'destroy'])->name('delete');
});


Route::as('pos.measurements.')->prefix('measurements')->group(function() {
    Route::get('/', [ MeasurementController::class, 'index'])->name('index');
    Route::post('/', [ MeasurementController::class, 'store'])->name('store');
    // Route::get('/{id}', [ MeasurementController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ MeasurementController::class, 'update'])->name('update');
    Route::delete('/{id}', [ MeasurementController::class, 'destroy'])->name('delete');
});


Route::as('pos.expenses.')->prefix('expenses')->group(function() {
    Route::get('/', [ ExpensesController::class, 'index'])->name('index');
    Route::post('/', [ ExpensesController::class, 'store'])->name('store');
    // Route::get('/{id}', [ ExpensesController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ ExpensesController::class, 'update'])->name('update');
    Route::delete('/{id}', [ ExpensesController::class, 'destroy'])->name('delete');
});

Route::as('pos.products.')->prefix('products')->group(function() {
    Route::get('/', [ ProductController::class, 'index'])->name('index');
    Route::post('/', [ ProductController::class, 'store'])->name('store');
    // Route::get('/{id}', [ ProductController::class, 'show'])->name('show');
    Route::match(['POST', 'PUT', 'PATCH'],'/{id}', [ ProductController::class, 'update'])->name('update');
    Route::delete('/{id}', [ ProductController::class, 'destroy'])->name('delete');
});
